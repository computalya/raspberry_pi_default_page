# README

simple replacement page for Pi devices

### Installation

### download template

`git clone git@gitlab.com:computalya/raspberry_pi_default_page.git`

### move related template files

`sudo mv raspberry_pi_default_page/default/* /var/www/html`

## remove old index file

`sudo rm /var/www/html/index.nginx-debian.html`


## Templates

### default

#### iPhone 8

![screenshots/iphone-8.png](screenshots/iphone-8.png)

#### 800x600

![screenshots/800x600.png](screenshots/800x600.png)

